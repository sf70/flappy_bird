export class Bird {

  constructor(startCords) {
    this.x = startCords[0];
    this.y = startCords[1];
    this.rotateDegree = 0;
    this.g = 1;
  }

  getX() {
    return this.x;
  }
  getY() {
    return this.y;
  }
  // угол поворота птицы при падении
  getRotateDegree() {
    return this.rotateDegree;
  }
  // ускорение падения птицы
  getG() {
    return this.g;
  }
  // спрайт птицы
  getSource(index) {
    const birdSource = {
      x: 276,
      y: Math.floor((index % 9) / 3) * 26 + 112,
      width: 35,
      height: 26,
    };
    return birdSource;
  }
  setX(x) {
    this.x = x;
  }
  setY(y) {
    this.y = y;
  }
  setRotateDegree(degree) {
    this.rotateDegree = degree;
  }
  setG(g) {
    this.g = g;
  }
  // действие с птичкой по нажатию на экран
  click() {
    // console.log("BIRD click");
    if (this.y > 0 || !this.y === 0) {
      this.y -= 50;
    }
    this.rotateDegree = -45; // угол поворота птицы при клике сбрасывается к дефолту
    this.g = 1; // коэффициент ускорения падения при клике сбрасывается к дефолту
    // console.log(this.x);
    // console.log(this.y);
  }
  restart(startCords) {
    this.x = startCords[0];
    this.y = startCords[1];
    this.rotateDegree = 0;
    this.g = 1;
  }
}

export class Pipe {
  constructor(max) {  // определяем конструктор класса
    let min = 24;
    this.y = Math.ceil(Math.random() * (max - min) + min);
    this.x = 0;
    this.passed = false;
  }
  getY() {  
    return this.y;
  }
  getPassed() {  
    return this.passed;
  }
  setPassed() {
    this.passed = true;
  }
  getID(){
    return this.id;
  }
  setID(id) {
    this.id = id;
  }
}