import * as sprite from "./sprite.js";
import * as classes from "./classes.js";
import * as conf from "./conf.js";


// шрифт для отображения счета игры
var myFont = new FontFace('myFont', 'url(assets/FB.ttf)');

myFont.load().then(function(font){

  document.fonts.add(font);
  console.log('Font loaded');

});

const bird = new classes.Bird(conf.birdStartCords);
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const BGSPEED = 2.1;
const SPEED = 3.1;
let index = 0;
let pipesIndex = 0;
let newGame = true;
let started = false;
let stopped = false;
let pipes = [];
let maxScore = +localStorage.getItem('fbMaxScore');
let score = 0;  
const spriteURL = './assets/sprite.png';
const restartURL = './assets/restart.png';

const img = new Image();
img.src = spriteURL;

const imgRestart = new Image();
imgRestart.src = restartURL;


canvas.addEventListener('mousedown', function(e) {
  if (!started) {
    started = true;
    newGame = false;
    console.log("STARTED");
    bird.click();
  } else {
    bird.click();
  }
  if (stopped) {
    getCursorPosition(canvas, e);
  }
});

// определение координат курсора и вызов перезапуска игры
function getCursorPosition(canvas, event) {
  const rect = canvas.getBoundingClientRect();
  const x = event.clientX - rect.left;
  const y = event.clientY - rect.top;
  console.log("x: " + x + " y: " + y);
  if (x > canvas.width / 2 - sprite.SpriteRestart.width / 2 && 
      x < canvas.width / 2 - sprite.SpriteRestart.width / 2 + sprite.SpriteRestart.width &&
      y > canvas.height / 2 - sprite.SpriteRestart.height / 2 && 
      y < canvas.height / 2 - sprite.SpriteRestart.height / 2 + sprite.SpriteRestart.height
  ) {
    console.log("11111111111111");
    restart();
  }
}
// функция перезапуска игры
function restart() {
  index = 0;
  pipesIndex = 0;
  newGame = false;
  started = true;
  stopped = false;
  pipes = [];
  maxScore = +localStorage.getItem('fbMaxScore');
  score = 0;
  bird.restart(conf.birdStartCords);
  for (let i = 0; i < 30; i++) {
    let pipe = new classes.Pipe(canvas.height / 3 * 2 - sprite.SpriteGround.height-170);
    pipe.setID(i);
    pipes.push(pipe);
  }
  render();
}

function setMaxScore(score) {
  if (score > maxScore) {
    localStorage.setItem('fbMaxScore', score);
  }
}


window.addEventListener('resize', canvasSize);
function canvasSize() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  bird.setX(Math.ceil(window.innerWidth/10));
  bird.setY(Math.ceil(window.innerHeight/3));
  window.requestAnimationFrame(render); 
}
// первичное заполнение массива с трубами
for (let i = 0; i < 30; i++) {
  let pipe = new classes.Pipe(canvas.height / 3 * 2 - sprite.SpriteGround.height-170);
  pipe.setID(i);
  pipes.push(pipe);
}

// Основная функция отрисовки фрейма
const render = () => {
  index += 0.5;
  
  const backgroudX = -((index * BGSPEED) % 275.5);
  const groundX = -((index * SPEED) % 210);
  if (started) {
    bird.setY(bird.getY() + 3 * bird.getG());
    bird.setG(bird.getG() + 0.05);
    if (bird.getRotateDegree() < 85) {
      bird.setRotateDegree(bird.getRotateDegree() + 3);
    }
  }
  
  // Отображение фона ================================================
  ctx.fillStyle = '#90CAF9';
  ctx.fillRect(0, 0, canvas.width, canvas.height/2);
  let spredFrames = Math.ceil(canvas.width/sprite.SpriteBackground.width)*2;
  //цикл для динамической отрисовки фона при изменении размера окна
  for (let i = 0; i < spredFrames; i++) {
    ctx.drawImage(
      img,
      sprite.SpriteBackground.x,
      sprite.SpriteBackground.y,
      sprite.SpriteBackground.width,
      sprite.SpriteBackground.height,
      
      backgroudX+i*sprite.SpriteBackground.width,
      canvas.height / 2 - sprite.SpriteBackground.height / 2,
      sprite.SpriteBackground.width,
      sprite.SpriteBackground.height
    );
    
  }
  ctx.fillStyle = '#82e38c';
  ctx.fillRect(0, canvas.height/2+sprite.SpriteBackground.height/2, canvas.width, canvas.height/2);
  //==================================================================

  // Отображение труб ================================================

  if (started) {
    
    const pipeX = -((pipesIndex * SPEED));//???

    pipesIndex += 0.5;
    for (const element of pipes) {
      //отрисовка тела трубы
      for (let i = 0; i <= canvas.height/sprite.SpritePipeBody.height; i++) {
        if (i*sprite.SpritePipeBody.height+sprite.SpritePipeBody.height < element.getY() + sprite.SpriteTopPipe.height *2 || i*sprite.SpritePipeBody.height > element.getY() +170+ sprite.SpriteTopPipe.height) {
          ctx.drawImage(
            img,

            sprite.SpritePipeBody.x,
            sprite.SpritePipeBody.y,
            sprite.SpritePipeBody.width,
            sprite.SpritePipeBody.height,

            canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4,
            i*sprite.SpritePipeBody.height-sprite.SpritePipeBody.height, //canvas.height / 2 - sprite.SpriteBackground.height / 2,
            sprite.SpritePipeBody.width,
            sprite.SpritePipeBody.height
          );
        }
      }
      // отрисовка верхней границы трубы
      ctx.drawImage(
        img,
        sprite.SpriteTopPipe.x,
        sprite.SpriteTopPipe.y,
        sprite.SpriteTopPipe.width,
        sprite.SpriteTopPipe.height,
  
        canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4,
        element.getY(),
        sprite.SpriteTopPipe.width,
        sprite.SpriteTopPipe.height
      );
      // отрисовка нижней границы трубы
      ctx.drawImage(
        img,
        sprite.SpriteBottomPipe.x,
        sprite.SpriteBottomPipe.y,
        sprite.SpriteBottomPipe.width,
        sprite.SpriteBottomPipe.height,
  
        canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4,
        element.getY()+170,
        sprite.SpriteBottomPipe.width,
        sprite.SpriteBottomPipe.height
      );
      if (bird.getX() + bird.getSource(0).width > canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4 + sprite.SpriteTopPipe.width/2 && !element.getPassed()) {
        score += 1;
        console.log("SCORE: ", score);
        element.setPassed();
      }
      if (bird.getX() + bird.getSource(0).width >= canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4 && 
      (bird.getY() < element.getY() + sprite.SpriteTopPipe.height || bird.getY()+ bird.getSource(0).height > element.getY() +150+ sprite.SpriteTopPipe.height) &&
      (bird.getX() + bird.getSource(0).width <= canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4+sprite.SpriteTopPipe.width || bird.getX() <= canvas.width+pipeX+element.getID()*sprite.SpriteTopPipe.width*4+sprite.SpriteTopPipe.width)
      ) {
        stopped = true;
      }

    }
    if (canvas.width+pipeX+pipes[0].getID()*sprite.SpriteTopPipe.width*4 < -55) {
      pipes = pipes.filter(item => item.id !== pipes[0].getID());
      let pipe = new classes.Pipe(canvas.height / 3 * 2 - sprite.SpriteGround.height-170);
      pipe.setID(pipes[pipes.length-1].getID()+1);
      pipes.push(pipe);
    }
  }
  //==================================================================
  // Отображение земли ===============================================
  ctx.fillStyle = '#ded895';
  ctx.fillRect(0, canvas.height / 3 * 2 - sprite.SpriteGround.height / 2, canvas.width, canvas.height/2);
  for (let i = 0; i < spredFrames; i++) {
    ctx.drawImage(
      img,

      sprite.SpriteGround.x,
      sprite.SpriteGround.y,
      sprite.SpriteGround.width,
      sprite.SpriteGround.height,

      groundX+i*sprite.SpriteGround.width,
      canvas.height / 3 * 2 - sprite.SpriteGround.height / 2,
      sprite.SpriteGround.width,
      sprite.SpriteGround.height
    );
  }
  //==================================================================
  // Отображение птицы ===============================================
  ctx.save();
  ctx.translate(bird.getX() + bird.getSource(index).width / 2, bird.getY() + bird.getSource(index).height / 2); 
  ctx.rotate(bird.getRotateDegree() * Math.PI / 180); 
  ctx.drawImage(
    img,

    bird.getSource(index).x,
    bird.getSource(index).y,
    bird.getSource(index).width,
    bird.getSource(index).height,

    0 - bird.getSource(index).width / 2,
    0 - bird.getSource(index).height / 2,
    bird.getSource(index).width,
    bird.getSource(index).height
  );
  ctx.restore();
  //==================================================================
  // Отображение старта игры =========================================
  if (newGame) {
    ctx.drawImage(
      img,
      
      sprite.SpriteTap.x,
      sprite.SpriteTap.y,
      sprite.SpriteTap.width,
      sprite.SpriteTap.height,
      
      canvas.width / 2 - sprite.SpriteTap.width / 2,
      canvas.height / 2 - sprite.SpriteTap.height / 2,
      sprite.SpriteTap.width,
      sprite.SpriteTap.height
    );
  }
  //==================================================================
  // Отрисовка счета текущей игры ====================================
  ctx.font = "50px myFont";
  ctx.fillStyle = "#ffffff";
  ctx.textBaseline = "middle";
  ctx.textAlign = "center"; 
  ctx.fillText("Best: " + maxScore + "\nCurrent: " + score, (canvas.width / 2), (canvas.height / 6 * 5));
  ctx.strokeStyle = "#000000";
  ctx.textBaseline = "middle";
  ctx.textAlign = "center"; 
  ctx.strokeText("Best: " + maxScore + "\nCurrent: " + score, (canvas.width / 2), (canvas.height / 6 * 5));
  //==================================================================
  // Проверка столкновения с землей
  if (bird.getY() + bird.getSource(index).height - 9 >= canvas.height / 3 * 2 - sprite.SpriteGround.height) {
    stopped = true;
  }

  // после завершения расчётов для текущего кадра сразу запускаем выполнение расчётов для следующего в случае если игра не остановилась
  if (!stopped) {
    window.requestAnimationFrame(render); 
  } else {
    // отрисовка кнопки перезапуска игры
    ctx.drawImage(
      imgRestart,
      
      sprite.SpriteRestart.x,
      sprite.SpriteRestart.y,
      sprite.SpriteRestart.width,
      sprite.SpriteRestart.height,
      
      canvas.width / 2 - sprite.SpriteRestart.width / 2,
      canvas.height / 2 - sprite.SpriteRestart.height / 2,
      sprite.SpriteRestart.width,
      sprite.SpriteRestart.height
    );
  }
  setMaxScore(score);
};

// отрисовка анимаций начнётся как только изображение будет загружено
img.onload = render;