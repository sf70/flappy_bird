export const SpriteBackground = {
  x: 0,
  y: 23,
  width: 275.5,
  height: 80,
};

export const SpriteGround = {
  x: 277,
  y: 0,
  width: 210,
  height: 12,
};

export const SpriteTopPipe = {
  x: 554,
  y: 376,
  width: 52,
  height: 24
};

export const SpritePipeBody = {
  x: 502,
  y: 25,
  width: 52,
  height: 24
};

export const SpriteBottomPipe = {
  x: 502,
  y: 0,
  width: 52,
  height: 24,
};

export const SpriteTap = {
  x: 28,
  y: 344,
  width: 118,
  height: 36,
};

export const SpriteRestart = {
  x: 0,
  y: 0,
  width: 215,
  height: 70,
};